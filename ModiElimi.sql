/*MODIFICACION DE DATOS*/
/*Modificamos un nombre de un juguete */

UPDATE joguina
SET nombre_joguina = 'Aprendo a multiplicar', edat = 10, capacitat_nen = 'Autoestima'
where id_joguina=26;

/*Modificamos juguetes que se terminaron y los ponemos a 0*/
UPDATE guardados
SET reserva_actual=0
where id_joguina in (11, 23, 18); 

/*ELIMINAR ALGUNOS DATOS*/

/* Elimnamos los nens que tienen un comportamiento malo*/
delete from nen
where codigo_nen in (select codigo_nen from nen where lower(comportament)= 'mala');

/*Eliminamos todos los juguetes que no tengan reservas*/
delete from joguina
where id_joguina in (select id_joguina from guardados where reserva_actual = 0); 
