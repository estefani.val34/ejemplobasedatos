/*Consultas simples*/

/*1.-Almacenes que hay en Barcelona. nombre_magatzem, direccion, nombre_poblacion, nombre_pais  */
select m.nombre_magatzem, m.direccion, p.nombre_poblacion, p.nombre_pais
from magatzem m, poblacions p
where p.nombre_magatzem=m.nombre_magatzem
and p.nombre_poblacion='Barcelona';

/*2.-Dime los juguetes equivalentes al juguete con id 7*/

select e.id_joguina2,j.nombre_joguina
from equivalente e, joguina j 
where e.id_joguina1=7 and 
e.id_joguina2=j.id_joguina
union
select e.id_joguina1,j.nombre_joguina
from equivalente e,joguina j
where e.id_joguina2=7 and
e.id_joguina1=j.id_joguina;


/*Consultas por agrupacion*/

/*1.-Dime el total de juguetes que tenemos en todos los almacenes de España*/

select  p.nombre_pais, sum(g.reserva_actual)
from magatzem m, poblacions p , guardados g, joguina j
where p.nombre_magatzem=m.nombre_magatzem and
	m.nombre_magatzem=g.nombre_magatzem and
	g.id_joguina=j.id_joguina and 
 	upper(p.nombre_pais)='ESPAÑA'
group by p.nombre_pais;

/*2.- Quiero saber los niños que ha pedido más cantidad de juguetes,superior a la media que han pedido el resto de niños*/

select pi.codigo_nen, n.nom, max(pi.cantidad)
from pide pi, cartes ca, nen n
where 	n.codigo_nen=ca.codigo_nen and 
	ca.numero_carta=pi.numero_carta
group by pi.codigo_nen, n.nom
having max(pi.cantidad)>(
		select avg(cantidad)
		from pide)
order by min(pi.cantidad) desc;

/*Consulta cruzando Tablas*/

/*1.-Los niños buenos de España y Portugal  entre 2002 y 2018 que juguetes les corresponde segun la edad. 
A partir de les dades de comportament dels nens, de les joguines comandes i de l'adequació 
de les joguines a l'edat dels nens, els Reis cada any assignen a cada nen les joguines que els regalessin.
*/

select n.codigo_nen,n.nom,n.comportament,n.nombre_pais,j.id_joguina,
h.año,j.edat as edat_joguina, to_char(SYSDATE,'YYYY')-to_char(n.data_naixement,'YYYY') as edad_nen
from nen n,corresponde co, historic h,joguina j
where lower(n.comportament)='buena' and n.nombre_pais in ('España', 'Portugal') and 
n.codigo_nen=co.codigo_nen and 
co.año=h.año and 
h.año between 2002 and 2018  and 
co.id_joguina=j.id_joguina and 
j.edat=to_char(SYSDATE,'YYYY')-to_char(n.data_naixement,'YYYY');

/*2.-¿Qué niños han enviado la carta a los reyes de forma
electronica y han pedido juguetes de tipo acuatico?*/

select ca.codigo_nen, ca.numero_carta,
ca.forma_envio , pi.id_joguina,j.tipus_esport
from cartes ca, pide pi, joguina j
where ca.forma_envio='Electronica' and 
ca.numero_carta=pi.numero_carta and 
pi.id_joguina=j.id_joguina and 
j.tipus_esport='Acuatic';

/*3.-Los juguetes que hay en los almacenes de los paises
con una latitud mayor a 45*/

select g.id_joguina, g.nombre_magatzem, pa.nombre_pais, pa.latitud
from guardados g, magatzem m, poblacions po, paisos pa
where g.nombre_magatzem=m.nombre_magatzem and
m.nombre_magatzem=po.nombre_magatzem and 
po.nombre_pais=pa.nombre_pais  and 
pa.latitud>45;

/*Consulta con subconsultas*/

/*1.-Los niños de  Barcelona que piden un numero de juguetes, mayor a la media de juguetes que piden el resto de niños en Barcelona*/

select ca.codigo_nen, n.nom, pi.cantidad
from pide pi,cartes ca, nen n
where  pi.numero_carta =ca.numero_carta
and pi.codigo_nen = n.codigo_nen
and pi.cantidad>(select round(avg(pi.cantidad),2)
		from pide pi, cartes ca, nen n
		where 	pi.numero_carta=ca.numero_carta and
			ca.codigo_nen=n.codigo_nen and 
			upper(n.nombre_poblacion)='BARCELONA' and 
			lower(n.nombre_pais)='españa' )
      order by pi.codigo_nen;

/*2.-Los niños buenos con la edad mayor a la media de edad*/

select codigo_nen,nom, comportament,
(to_char(sysdate,'YYYY')-to_char(data_naixement,'YYYY')) as edad
from nen
  where lower(comportament)='buena'and 

((to_char(sysdate,'YYYY')-to_char(data_naixement,'YYYY')))>(

select avg(to_char(sysdate,'YYYY')-to_char(data_naixement,'YYYY'))
from nen
where lower(comportament)='buena'
group by comportament                                      )
			
order by 4;

/*Consulta utilizant UNION*/

/*1.-Los pages que hayan trabajado el año 2017 y los pages que hagan trabajado el año 2018*/

select e.patge_id,p.nombre_patge,e.año
from encarga e, patges p
where  	e.patge_id=p.patge_id and
	e.año=2017
UNION
select e.patge_id,p.nombre_patge,e.año
from encarga e, patges p
where  	e.patge_id=p.patge_id and
	e.año=2018;
 
/*Consulta utilizant INTERSECT*/

/*1.-Los pages que tengan en su nombre una c y una r*/

	select patge_id, nombre_patge
from patges
where upper(nombre_patge) like '%C%'
INTERSECT
select patge_id, nombre_patge
from patges
where upper(nombre_patge) like '%R%';


