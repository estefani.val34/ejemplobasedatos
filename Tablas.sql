/*BORRAR TABLAS QUE COINCIDAN*/
drop table "HR"."CARTES" cascade constraints PURGE;
drop table "HR"."CORRESPONDE" cascade constraints PURGE;
drop table "HR"."ENCARGA"cascade constraints PURGE;
drop table "HR"."GUARDADOS"cascade constraints PURGE;
drop table "HR"."HISTORIC"cascade constraints PURGE;
drop table "HR"."JOGUINA"cascade constraints PURGE;
drop table "HR"."MAGATZEM"cascade constraints PURGE;
drop table "HR"."NEN"cascade constraints PURGE;
drop table "HR"."PAISOS"cascade constraints PURGE;
drop table "HR"."PATGES"cascade constraints PURGE;
drop table "HR"."PIDE"cascade constraints PURGE;
drop table "HR"."POBLACIONS"cascade constraints PURGE;
drop table "HR"."EQUIVALENTE"cascade constraints PURGE;
DROP sequence joguina_id_seq;
DROP sequence cartes_num_seq;
DROP sequence nen_cod_seq;
DROP sequence patge_id_seq;
drop view "HR"."VIEW_HABILITATS_SOCIALS";
drop view "HR"."VIEW_PATGES_BCN";


/*CREACION DE LAS TABLAS*/

create table joguina
(
id_joguina number(10),
nombre_joguina varchar2(100) not null,
descripcio varchar2(300),
edat number(10) not null,
capacitat_nen varchar2(30), 
tipus_esport varchar2(20),
constraint jogui_nom_pk primary key (id_joguina),
constraint jogui_edat_ck check(edat<16),
constraint edu_cap_ck check (capacitat_nen in('Psicomotricitat', 'Autoestima', 'Habilitats Socials')),
constraint esp_tipus_ck check(tipus_esport in('Equip', 'Pilota', 'Acuatic'))
);

create table equivalente  
(
id_joguina1 number(10) not null ,
id_joguina2 number(10) not null ,
constraint eq_jo12_pk primary key (id_joguina1,id_joguina2),
constraint eq_jo1_fk foreign key (id_joguina1)
references joguina(id_joguina) on delete cascade,
constraint eq_jo2_fk foreign key (id_joguina2)
references joguina(id_joguina) on delete cascade
);

create table paisos
(
nombre_pais varchar2(70),
nº_nens number(10),
latitud number(8,4),
longitud number(8,4),
constraint ps_nom_pk primary key (nombre_pais)
);
create table magatzem 
(
nombre_magatzem varchar2(25),
direccion varchar2(100) not null,
constraint mg_nom_pk primary key (nombre_magatzem)
);
create table patges 
(
patge_id number(10) primary key,
nombre_patge varchar2(50),
idioma varchar2(50) default 'Inglés'
);
create table historic
(
año number(10) default to_char(sysdate,'YYYY'),
constraint hs_año_pk primary key (año),
constraint hs_año_ck check (año>2000)
);
create table poblacions
(
nombre_poblacion varchar2(70) not null unique,
nombre_pais varchar2(70) not null unique,
nombre_magatzem varchar2(25) ,
constraint pob_nom_pk primary key (nombre_poblacion,nombre_pais),
constraint pob_nomp_fk foreign key (nombre_pais)  
references paisos(nombre_pais) on delete cascade,
constraint pob_mag_fk foreign key (nombre_magatzem) 
references magatzem(nombre_magatzem) on delete cascade
);
create table nen
(
codigo_nen number(10) not null unique,
nom varchar2(100) not null,
data_naixement date default sysdate,
comportament varchar2(70),
nombre_poblacion varchar2(70) not null,
nombre_pais varchar2(70) not null, 
constraint nen_cod_pk primary key (codigo_nen,nombre_poblacion,nombre_pais),
constraint nen_comp_ck check(comportament in('Buena', 'Normal', 'Mala')),
constraint nen_pob_fk foreign key (nombre_poblacion,nombre_pais) references poblacions (nombre_poblacion,nombre_pais) on delete cascade
);

create table cartes 
(
numero_carta number(10) not null unique,
año number(10) default to_char(sysdate,'YYYY'),
forma_envio varchar2(15),
codigo_nen number(10) not null unique,
constraint car_num_pk primary key (numero_carta,codigo_nen),
constraint car_form_ck check (forma_envio in('Postal' , 'Electronica')),
constraint nen_cod_fk foreign key (codigo_nen) references nen(codigo_nen) on delete cascade
);
create table pide 
(
id_joguina number(10) not null unique,
numero_carta number(10) not null unique,
codigo_nen number(10) not null unique,
cantidad number(10),
constraint pid_nom_pk primary key
(id_joguina,numero_carta,codigo_nen),
constraint pid_nom_fk foreign key (id_joguina) 
references joguina(id_joguina) ON DELETE CASCADE,
constraint pid_num_fk foreign key (numero_carta) 
references cartes(numero_carta) on delete cascade,
constraint pid_cod_fk foreign key (codigo_nen)
references nen(codigo_nen) on delete cascade

);
create table guardados
(
id_joguina number(10) not null unique,
nombre_magatzem varchar2(25) not null,
reserva_actual number(10),
constraint gr_nom_pk primary key (id_joguina, nombre_magatzem),
constraint gr_nomj_fk foreign key (id_joguina) 
references joguina(id_joguina) on delete cascade,
constraint gr_nomm_fk foreign key (nombre_magatzem)
references magatzem(nombre_magatzem) on delete cascade
);

create table encarga
(
patge_id number(10) not null unique,
año number(10) default to_char(sysdate,'YYYY') not null,
nombre_poblacion varchar2(70),
nombre_pais varchar2(70),
constraint en_pt_pk primary key (patge_id, año),
constraint en_pt_fk foreign key (patge_id)
references patges(patge_id) on delete cascade,
constraint en_año_fk foreign key (año)
references historic(año) on delete cascade,
constraint en_año_ck check (año>2000),
constraint en_nom_fk foreign key (nombre_poblacion,nombre_pais) 
references poblacions (nombre_poblacion,nombre_pais) on delete cascade
);

create table corresponde
(
id_joguina number(10) not null unique,
codigo_nen number(10) not null unique,
año number(10) default to_char(sysdate,'YYYY') not null,
constraint cr_nomj_pk primary key (id_joguina, codigo_nen),
constraint cr_nomj_fk foreign key (id_joguina) 
references joguina(id_joguina) on delete cascade,
constraint cr_cg_fk foreign key (codigo_nen)
references nen(codigo_nen) on delete cascade,
constraint cr_año_fk foreign key (año)
references historic(año) on delete cascade,
constraint cr_año_ck check (año>2000)
);

/*SECUENCIAS*/
create sequence joguina_id_seq
increment by 1
start with 1
nocycle;
create sequence patge_id_seq
increment by 1
start with 1
nocycle;
create sequence nen_cod_seq
increment by 1
start with 1
nocycle;
create sequence cartes_num_seq
increment by 1
start with 1
nocycle;

/* REGRISTO DE TODOS LOS DATOS*/

/*deportivos pelota*/
INSERT INTO joguina (id_joguina, nombre_joguina, descripcio, edat, tipus_esport) VALUES
(joguina_id_seq.nextval, 'T-Ball','El juguete saltarín más divertido. El Tball permite efectuar saltos impresionantes.', 6, 'Pilota');
INSERT INTO joguina (id_joguina, nombre_joguina, descripcio, edat, tipus_esport) VALUES
(joguina_id_seq.nextval, 'Canasta de Baloncesto', 'Juega a baloncesto sin salir de casa! Esta mini canasta es ideal para pasar un rato divertido.', 9, 'Pilota');
INSERT INTO joguina (id_joguina, nombre_joguina, descripcio, edat, tipus_esport) VALUES
(joguina_id_seq.nextval, 'Cuerda de Saltar Varios Colores', 'Salta con un amigo o amiga y utiliza tu imaginación descubriendo diferentes maneras de saltar.', 6, 'Pilota');
INSERT INTO joguina (id_joguina, nombre_joguina, descripcio, edat, tipus_esport) VALUES
(joguina_id_seq.nextval, 'Chicco Fit y Fun Goal League', 'La portería de fútbol se activa cuando tu hijo marca un gol, encendiéndose luces y sonidos', 3, 'Pilota');
INSERT INTO joguina (id_joguina, nombre_joguina, descripcio, edat, tipus_esport) VALUES
(joguina_id_seq.nextval, 'Bebé Vip Pelota Blandita con Actividades', 'Pelota blandita musical con diferentes texturas y actividades para que tu bebé se lo pase en grande', 1, 'Pilota');

/*deportivos acuaticos*/
INSERT INTO joguina (id_joguina, nombre_joguina, descripcio, edat, tipus_esport) VALUES
(joguina_id_seq.nextval, 'Super Wings Pistola de Agua', 'Combate el calor con la super pistola de agua de Super Wings. La pistola mide 40 cm ', 6, 'Acuatic');
INSERT INTO joguina (id_joguina, nombre_joguina, descripcio, edat, tipus_esport) VALUES
(joguina_id_seq.nextval, 'Piscina Pez', 'Una piscina muy divertida y a la vez refrescante. Tu bebé se divertirá contigo.',3, 'Acuatic');
INSERT INTO joguina (id_joguina, nombre_joguina, descripcio, edat, tipus_esport) VALUES
(joguina_id_seq.nextval, 'Hula Hops para Piscina', 'Un juego muy divertido para bucear. ¡Tienes que pasarlos los tres!', 6, 'Acuatic');
INSERT INTO joguina (id_joguina, nombre_joguina, descripcio, edat, tipus_esport) VALUES
(joguina_id_seq.nextval, 'Juego Palos Acuáticos', 'Un divertido juego para la piscina con palos para poner el el fondo.', 9, 'Acuatic');
INSERT INTO joguina (id_joguina, nombre_joguina, descripcio, edat, tipus_esport) VALUES
(joguina_id_seq.nextval, 'Nincocean Tiburón', 'Controla el divertido tiburón en todas las direcciones con tu mando de radio control.',8, 'Acuatic');

/*deportivos en equipo*/
INSERT INTO joguina (id_joguina, nombre_joguina, descripcio, edat, tipus_esport) VALUES
(joguina_id_seq.nextval, 'T-Toca Línea 4', 'Si quieres ganar consigue 4 fichas en línea de tu color antes que tu oponente', 9, 'Equip');
INSERT INTO joguina (id_joguina, nombre_joguina, descripcio, edat, tipus_esport) VALUES
(joguina_id_seq.nextval, 'Sin Palabras', '¡El juego para toda la familia que te dejará mudo! Gesticula, dibuja, pinta, interpreta, adivina', 9, 'Equip');
INSERT INTO joguina (id_joguina, nombre_joguina, descripcio, edat, tipus_esport) VALUES
(joguina_id_seq.nextval, 'Gestos', 'El mejor juego de mímica para toda la familia. Sé el más rápido porque la claqueta se irá tragando tus tarjetas.', 12, 'Equip');
INSERT INTO joguina (id_joguina, nombre_joguina, descripcio, edat, tipus_esport) VALUES
(joguina_id_seq.nextval, 'Monopoly Junior', 'Los más pequeños se divertirán con este nuevo Monopoly. Con nuevas fichas infantiles con personalidad!', 7, 'Equip');
INSERT INTO joguina (id_joguina, nombre_joguina, descripcio, edat, tipus_esport) VALUES
(joguina_id_seq.nextval, 'Juego de Mesa Misterio', 'Con Misterio volverás a jugar al emocionante juego de los detectives, rodeado de monstruos y estancias misteriosas', 10, 'Equip');

/* educativos psicomotricitat*/
INSERT INTO joguina (id_joguina, nombre_joguina, descripcio, edat, capacitat_nen) VALUES
(joguina_id_seq.nextval, 'Paw Patrol Maletín Educativo', 'Juego ideal para llevar a donde los niños quieran gracias a su práctica mesita portátil y a su boli interactivo', 6, 'Psicomotricitat');
INSERT INTO joguina (id_joguina, nombre_joguina, descripcio, edat, capacitat_nen) VALUES
(joguina_id_seq.nextval, 'Educativos Mosaicos con Pegatinas', 'Juguete educativo con el que tu niño o niña se iniciará en las manualidades básicas y practicará ejercicios', 6, 'Psicomotricitat');
INSERT INTO joguina (id_joguina, nombre_joguina, descripcio, edat, capacitat_nen) VALUES
(joguina_id_seq.nextval, 'Cleo Kit Educativo', 'Incluye rompecabezas 9 cubos, dominó y juego de memoria', 6, 'Psicomotricitat');
INSERT INTO joguina (id_joguina, nombre_joguina, descripcio, edat, capacitat_nen) VALUES
(joguina_id_seq.nextval, 'Baby Clementoni Max Camión Educativo', 'Sansón es un juguete 3 en 1: es un divertido camión para conducir, práctico para los primeros pasos, y actividades educativas', 3, 'Psicomotricitat');
INSERT INTO joguina (id_joguina, nombre_joguina, descripcio, edat, capacitat_nen) VALUES
(joguina_id_seq.nextval, 'Baby Clementoni Mesa Educativa Multijuegos', 'Mesa con actividades eductaivas y efectos luminosos.', 3, 'Psicomotricitat');

/*educativos autoestima*/
INSERT INTO joguina (id_joguina, nombre_joguina, descripcio, edat, capacitat_nen) VALUES
(joguina_id_seq.nextval, 'Paw Patrol Skye Maletín Educativo', 'Juego ideal para llevar a donde los niños quieran gracias a su práctica mesita portátil y a su boli interactivo', 6, 'Autoestima');
INSERT INTO joguina (id_joguina, nombre_joguina, descripcio, edat, capacitat_nen) VALUES
(joguina_id_seq.nextval, 'Baby Clementoni Tren Educativo 123', '¡Un colorido tren diseñado para que los más pequeños entren en el mundo de la diversión a toda velocidad', 1, 'Autoestima');
INSERT INTO joguina (id_joguina, nombre_joguina, descripcio, edat, capacitat_nen) VALUES
(joguina_id_seq.nextval, 'Nenittos Banco de Trabajo de Madera', 'Un banco de trabajo de madera para construir todo lo que imagines', 6, 'Autoestima');
INSERT INTO joguina (id_joguina, nombre_joguina, descripcio, edat, capacitat_nen) VALUES
(joguina_id_seq.nextval, 'Bebé Vip Pollitos Apilables', 'Qué bien se lo pasará tu bebé con estos simpáticos pollitos apilables ', 1, 'Autoestima');
INSERT INTO joguina (id_joguina, nombre_joguina, descripcio, edat, capacitat_nen) VALUES
(joguina_id_seq.nextval, 'Nenittos Cubo Descubrimientos con Sonido', 'En cada lado una sorpresa! Con este cubo de Nenittos te divertirás muchísimo', 3, 'Autoestima');

/*educativos habilidad social*/
INSERT INTO joguina (id_joguina, nombre_joguina, descripcio, edat, capacitat_nen) VALUES
(joguina_id_seq.nextval, 'Yo Aprendo a Contar', 'Maletín educativo para aprender a contar y las primeras operaciones matemáticas.', 6, 'Habilitats Socials');
INSERT INTO joguina (id_joguina, nombre_joguina, descripcio, edat, capacitat_nen) VALUES
(joguina_id_seq.nextval, 'Dessineo Aprende a Dibujar Paso a Paso', 'Maletín educativo para aprender a dibujar en cuatro sencillos pasos', 6, 'Habilitats Socials');
INSERT INTO joguina (id_joguina, nombre_joguina, descripcio, edat, capacitat_nen) VALUES
(joguina_id_seq.nextval, 'Conector Educativo escula', ' Los temas que gustan a lo más pequeños y los primeros temas de la escuela', 6, 'Habilitats Socials');
INSERT INTO joguina (id_joguina, nombre_joguina, descripcio, edat, capacitat_nen) VALUES
(joguina_id_seq.nextval, 'Conector Educativo autos', 'base multiconectora, cabezal con voces y sonidos.', 9, 'Habilitats Socials');
INSERT INTO joguina (id_joguina, nombre_joguina, descripcio, edat, capacitat_nen) VALUES
(joguina_id_seq.nextval, 'Peppa Pig Kit Educativo', 'Kit Educativo Peppa Pig. Incluye rompecabezas 9 cubos, dominó y juego de memoria.', 6, 'Habilitats Socials');
/*Equivalente*/
INSERT INTO equivalente
(id_joguina1, id_joguina2) VALUES
(1, 2);
INSERT INTO equivalente
(id_joguina1, id_joguina2) VALUES
(1, 7);
INSERT INTO equivalente
(id_joguina1, id_joguina2) VALUES
(8, 9);
INSERT INTO equivalente
(id_joguina1, id_joguina2) VALUES
(7, 4);
INSERT INTO equivalente
(id_joguina1, id_joguina2) VALUES
(25, 3);
INSERT INTO equivalente
(id_joguina1, id_joguina2) VALUES
(9, 10);
INSERT INTO equivalente
(id_joguina1, id_joguina2) VALUES
(8, 20);
INSERT INTO equivalente
(id_joguina1, id_joguina2) VALUES
(23, 8);
INSERT INTO equivalente
(id_joguina1, id_joguina2) VALUES
(26, 25);
INSERT INTO equivalente
(id_joguina1, id_joguina2) VALUES
(14, 8);
INSERT INTO equivalente
(id_joguina1, id_joguina2) VALUES
(9, 7);
INSERT INTO equivalente
(id_joguina1, id_joguina2) VALUES
(25, 17);
INSERT INTO equivalente
(id_joguina1, id_joguina2) VALUES
(14, 24);
INSERT INTO equivalente
(id_joguina1, id_joguina2) VALUES
(25, 4);
INSERT INTO equivalente
(id_joguina1, id_joguina2) VALUES
(1, 9);
INSERT INTO equivalente
(id_joguina1, id_joguina2) VALUES
(28, 17);
INSERT INTO equivalente
(id_joguina1, id_joguina2) VALUES
(18, 14);
INSERT INTO equivalente
(id_joguina1, id_joguina2) VALUES
(21, 2);
INSERT INTO equivalente
(id_joguina1, id_joguina2) VALUES
(30, 25);
INSERT INTO equivalente
(id_joguina1, id_joguina2) VALUES
(15, 11);
INSERT INTO equivalente
(id_joguina1, id_joguina2) VALUES
(8, 10);


/*paisos*/
INSERT INTO paisos 
(nombre_pais, latitud, longitud) VALUES
('España', 41.3818, 67.0093);
INSERT INTO paisos 
(nombre_pais, latitud, longitud) VALUES
('Francia', 13.8509, 23.9852);
INSERT INTO paisos 
(nombre_pais, latitud, longitud) VALUES
('Portugal', 90.2532, 12.9874);
INSERT INTO paisos 
(nombre_pais, latitud, longitud) VALUES
('Argentina', 34.0009, 32.9987);
INSERT INTO paisos 
(nombre_pais, latitud, longitud) VALUES
('Colombia', 45.9312, 98.9898);
INSERT INTO paisos 
(nombre_pais, latitud, longitud) VALUES
('Italia', 12.3954, 09.9873);
INSERT INTO paisos 
(nombre_pais, latitud, longitud) VALUES
('Venezuela', 56.0753, 10.0098);

/*magatzem*/
INSERT INTO MAGATZEM
(nombre_magatzem, direccion) VALUES
('Magtzem-Barcelona', 'Calle Princep Jordi Nº98 08039');
INSERT INTO MAGATZEM
(nombre_magatzem, direccion) VALUES
('Magtzem-Paris', 'Calle Courbevoie Nº09 09900');
INSERT INTO MAGATZEM
(nombre_magatzem, direccion) VALUES
('Magtzem-Lisboa', 'Calle Filipi Folque Nº652 77632');
INSERT INTO MAGATZEM
(nombre_magatzem, direccion) VALUES
('Magtzem-BAires', 'Calle Almafuerte Nº05 23644');
INSERT INTO MAGATZEM
(nombre_magatzem, direccion) VALUES
('Magtzem-Bogota', 'Calle Camelia Nº76 10094');
INSERT INTO MAGATZEM
(nombre_magatzem, direccion) VALUES
('Magtzem-Roma', 'Calle Taranto Nº44 08995');
INSERT INTO MAGATZEM
(nombre_magatzem, direccion) VALUES
('Magtzem-Caracas', 'Calle Los Mangos Nº01 66533');
/*Patge*/
INSERT INTO patges 
(patge_id, nombre_patge, idioma) VALUES
(patge_id_seq.nextval, 'Pablo Vargas Martinez', 'Catalan');
INSERT INTO patges 
(patge_id, nombre_patge) VALUES
(patge_id_seq.nextval, 'Juan José Tomas Ibañez');
INSERT INTO patges 
(patge_id, nombre_patge, idioma) VALUES
(patge_id_seq.nextval, 'Marc Pascual Cano', 'Portugues');
INSERT INTO patges 
(patge_id, nombre_patge) VALUES
(patge_id_seq.nextval, 'Omar Sanchez Leon');
INSERT INTO patges 
(patge_id, nombre_patge, idioma) VALUES
(patge_id_seq.nextval, 'Bruno Navarro Calvo', 'Español');
INSERT INTO patges 
(patge_id, nombre_patge) VALUES
(patge_id_seq.nextval, 'Daniel Fernandez Jimenez');
INSERT INTO patges 
(patge_id, nombre_patge, idioma) VALUES
(patge_id_seq.nextval, 'Luis Martinez Delgado', 'Español');

/*año*/
INSERT INTO historic
(año) VALUES 
(2001);
INSERT INTO historic
(año) VALUES 
(2002);
INSERT INTO historic
(año) VALUES 
(2003);
INSERT INTO historic
(año) VALUES 
(2004);
INSERT INTO historic
(año) VALUES 
(2005);
INSERT INTO historic
(año) VALUES 
(2006);
INSERT INTO historic
(año) VALUES 
(2007);
INSERT INTO historic
(año) VALUES 
(2008);
INSERT INTO historic
(año) VALUES 
(2009);
INSERT INTO historic
(año) VALUES 
(2010);
INSERT INTO historic
(año) VALUES 
(2011);
INSERT INTO historic
(año) VALUES 
(2012);
INSERT INTO historic
(año) VALUES 
(2013);
INSERT INTO historic
(año) VALUES 
(2014);
INSERT INTO historic
(año) VALUES 
(2015);
INSERT INTO historic
(año) VALUES 
(2016);
INSERT INTO historic
(año) VALUES 
(2017);
INSERT INTO historic
(año) VALUES 
(2018);
/*poblacions*/
INSERT INTO poblacions
(nombre_poblacion, nombre_pais, nombre_magatzem) VALUES
('Barcelona', 'España', 'Magtzem-Barcelona');
INSERT INTO poblacions
(nombre_poblacion, nombre_pais, nombre_magatzem) VALUES
('Paris', 'Francia', 'Magtzem-Paris');
INSERT INTO poblacions
(nombre_poblacion, nombre_pais, nombre_magatzem) VALUES
('Lisboa', 'Portugal', 'Magtzem-Lisboa');
INSERT INTO poblacions
(nombre_poblacion, nombre_pais, nombre_magatzem) VALUES
('Buenos Aires', 'Argentina', 'Magtzem-BAires');
INSERT INTO poblacions
(nombre_poblacion, nombre_pais, nombre_magatzem) VALUES
('Bogota', 'Colombia', 'Magtzem-Bogota');
INSERT INTO poblacions
(nombre_poblacion, nombre_pais, nombre_magatzem) VALUES
('Roma', 'Italia', 'Magtzem-Roma');
INSERT INTO poblacions
(nombre_poblacion, nombre_pais, nombre_magatzem) VALUES
('Caracas', 'Venezuela', 'Magtzem-Caracas');
/*NEN*/
INSERT INTO nen
(codigo_nen, nom, data_naixement, comportament, nombre_poblacion, nombre_pais) VALUES
(nen_cod_seq.nextval, 'Ian Moreno Moreno',to_date('20030926', 'YYYYMMDD'), 'Buena', 'Barcelona', 'España');
INSERT INTO nen
(codigo_nen, nom, data_naixement, comportament, nombre_poblacion, nombre_pais) VALUES
(nen_cod_seq.nextval, 'Aitor Sanchez Diez',to_date('20051111', 'YYYYMMDD'), 'Normal', 'Barcelona', 'España'); 
INSERT INTO nen
(codigo_nen, nom, data_naixement, comportament, nombre_poblacion, nombre_pais) VALUES
(nen_cod_seq.nextval, 'Luis Castro Gomez',to_date('20180923', 'YYYYMMDD'), 'Mala', 'Barcelona', 'España'); 
INSERT INTO nen
(codigo_nen, nom, data_naixement, comportament, nombre_poblacion, nombre_pais) VALUES
(nen_cod_seq.nextval, 'Adria Marin Moreno',to_date('20050906', 'YYYYMMDD'), 'Buena', 'Barcelona', 'España'); 
INSERT INTO nen
(codigo_nen, nom, data_naixement, comportament, nombre_poblacion, nombre_pais) VALUES
(nen_cod_seq.nextval, 'Daniel Nuñez Moreno',to_date('20030823', 'YYYYMMDD'), 'Normal', 'Barcelona', 'España'); 
INSERT INTO nen
(codigo_nen, nom, data_naixement, comportament, nombre_poblacion, nombre_pais) VALUES
(nen_cod_seq.nextval, 'Óscar Cabrera Parra',to_date('20090909', 'YYYYMMDD'), 'Mala', 'Paris', 'Francia'); 
INSERT INTO nen
(codigo_nen, nom, data_naixement, comportament, nombre_poblacion, nombre_pais) VALUES
(nen_cod_seq.nextval, 'Luis Romero Gallego',to_date('20101009', 'YYYYMMDD'), 'Buena', 'Paris', 'Francia'); 
INSERT INTO nen
(codigo_nen, nom, data_naixement, comportament, nombre_poblacion, nombre_pais) VALUES
(nen_cod_seq.nextval, 'Adrián Vazquez Serrano',to_date('20090807', 'YYYYMMDD'), 'Normal', 'Paris', 'Francia'); 
INSERT INTO nen
(codigo_nen, nom, data_naixement, comportament, nombre_poblacion, nombre_pais) VALUES
(nen_cod_seq.nextval, 'Nicolás Gutierrez Gimenez',to_date('20121205', 'YYYYMMDD'), 'Mala', 'Paris', 'Francia'); 
INSERT INTO nen
(codigo_nen, nom, data_naixement, comportament, nombre_poblacion, nombre_pais) VALUES
(nen_cod_seq.nextval, 'Santiago Alvarez Marin',to_date('20130812', 'YYYYMMDD'), 'Buena', 'Lisboa', 'Portugal'); 
INSERT INTO nen
(codigo_nen, nom, data_naixement, comportament, nombre_poblacion, nombre_pais) VALUES
(nen_cod_seq.nextval, 'Éric Navarro Muñoz',to_date('20180104', 'YYYYMMDD'), 'Normal', 'Lisboa', 'Portugal');
INSERT INTO nen
(codigo_nen, nom, data_naixement, comportament, nombre_poblacion, nombre_pais) VALUES
(nen_cod_seq.nextval, 'Óliver Caballero Diaz',to_date('20050908', 'YYYYMMDD'), 'Mala', 'Lisboa', 'Portugal'); 
INSERT INTO nen
(codigo_nen, nom, data_naixement, comportament, nombre_poblacion, nombre_pais) VALUES
(nen_cod_seq.nextval, 'Ian Martin Iglesias',to_date('20030529', 'YYYYMMDD'), 'Buena', 'Lisboa', 'Portugal'); 
INSERT INTO nen
(codigo_nen, nom, data_naixement, comportament, nombre_poblacion, nombre_pais) VALUES
(nen_cod_seq.nextval, 'Ángel Nuñez Moreno',to_date('20101009', 'YYYYMMDD'), 'Normal', 'Buenos Aires', 'Argentina'); 
INSERT INTO nen
(codigo_nen, nom, data_naixement, comportament, nombre_poblacion, nombre_pais) VALUES
(nen_cod_seq.nextval, 'Marco Gallego Benitez',to_date('20140510', 'YYYYMMDD'), 'Mala', 'Buenos Aires', 'Argentina'); 
INSERT INTO nen
(codigo_nen, nom, data_naixement, comportament, nombre_poblacion, nombre_pais) VALUES
(nen_cod_seq.nextval, 'Lucas Morales Hidalgo',to_date('20170423', 'YYYYMMDD'), 'Buena', 'Buenos Aires', 'Argentina'); 
INSERT INTO nen
(codigo_nen, nom, data_naixement, comportament, nombre_poblacion, nombre_pais) VALUES
(nen_cod_seq.nextval, 'Juan José Ruiz Prieto',to_date('20140928', 'YYYYMMDD'), 'Normal', 'Buenos Aires', 'Argentina'); 
INSERT INTO nen
(codigo_nen, nom, data_naixement, comportament, nombre_poblacion, nombre_pais) VALUES
(nen_cod_seq.nextval, 'Joaquín Vidal Herrero',to_date('20110121', 'YYYYMMDD'), 'Mala', 'Bogota', 'Colombia'); 
INSERT INTO nen
(codigo_nen, nom, data_naixement, comportament, nombre_poblacion, nombre_pais) VALUES
(nen_cod_seq.nextval, 'Santiago Alvarez Caballero',to_date('20120403', 'YYYYMMDD'), 'Buena', 'Bogota', 'Colombia'); 
INSERT INTO nen
(codigo_nen, nom, data_naixement, comportament, nombre_poblacion, nombre_pais) VALUES
(nen_cod_seq.nextval, 'César Sanchez Ortiz',to_date('20180121', 'YYYYMMDD'), 'Normal', 'Bogota', 'Colombia'); 
INSERT INTO nen
(codigo_nen, nom, data_naixement, comportament, nombre_poblacion, nombre_pais) VALUES
(nen_cod_seq.nextval, 'Samuel Reyes Gallego',to_date('20170123', 'YYYYMMDD'), 'Mala', 'Bogota', 'Colombia'); 
INSERT INTO nen
(codigo_nen, nom, data_naixement, comportament, nombre_poblacion, nombre_pais) VALUES
(nen_cod_seq.nextval, 'Fernando Garrido Blanco',to_date('20150323', 'YYYYMMDD'), 'Buena', 'Roma', 'Italia'); 
INSERT INTO nen
(codigo_nen, nom, data_naixement, comportament, nombre_poblacion, nombre_pais) VALUES
(nen_cod_seq.nextval, 'Gerard Gimenez Garcia',to_date('20070909', 'YYYYMMDD'), 'Normal', 'Roma', 'Italia'); 
INSERT INTO nen
(codigo_nen, nom, data_naixement, comportament, nombre_poblacion, nombre_pais) VALUES
(nen_cod_seq.nextval, 'Jorge Mora Delgado',to_date('20120605', 'YYYYMMDD'), 'Mala', 'Roma', 'Italia'); 
INSERT INTO nen
(codigo_nen, nom, data_naixement, comportament, nombre_poblacion, nombre_pais) VALUES
(nen_cod_seq.nextval, 'Arnau Soler Nuñez',to_date('20101025', 'YYYYMMDD'), 'Buena', 'Roma', 'Italia'); 
INSERT INTO nen
(codigo_nen, nom, data_naixement, comportament, nombre_poblacion, nombre_pais) VALUES
(nen_cod_seq.nextval, 'Rodrigo Marquez Flores',to_date('20180506', 'YYYYMMDD'), 'Normal', 'Caracas', 'Venezuela'); 
INSERT INTO nen
(codigo_nen, nom, data_naixement, comportament, nombre_poblacion, nombre_pais) VALUES
(nen_cod_seq.nextval, 'Adrián Cortes Ortega',to_date('20030810', 'YYYYMMDD'), 'Mala', 'Caracas', 'Venezuela'); 
INSERT INTO nen
(codigo_nen, nom, data_naixement, comportament, nombre_poblacion, nombre_pais) VALUES
(nen_cod_seq.nextval, 'Joel Martin Fuentes',to_date('20040528', 'YYYYMMDD'), 'Buena', 'Caracas', 'Venezuela'); 
INSERT INTO nen
(codigo_nen, nom, data_naixement, comportament, nombre_poblacion, nombre_pais) VALUES
(nen_cod_seq.nextval, 'Manuel Vargas Medina',to_date('20070923', 'YYYYMMDD'), 'Normal', 'Caracas', 'Venezuela'); 
INSERT INTO nen
(codigo_nen, nom, data_naixement, comportament, nombre_poblacion, nombre_pais) VALUES
(nen_cod_seq.nextval, 'Javier Cortes Moreno',to_date('20130925', 'YYYYMMDD'), 'Mala', 'Caracas', 'Venezuela'); 

/*Cartes*/
INSERT INTO cartes 
(numero_carta, año, forma_envio, codigo_nen) VALUES
(cartes_num_seq.nextval, 2016, 'Postal', 4);
INSERT INTO cartes 
(numero_carta, año, forma_envio, codigo_nen) VALUES
(cartes_num_seq.nextval, 2018, 'Electronica', 3);
INSERT INTO cartes 
(numero_carta, año, forma_envio, codigo_nen) VALUES
(cartes_num_seq.nextval, 2010, 'Postal', 2);
INSERT INTO cartes 
(numero_carta, año, forma_envio, codigo_nen) VALUES
(cartes_num_seq.nextval, 2009, 'Electronica', 1);
INSERT INTO cartes 
(numero_carta, año, forma_envio, codigo_nen) VALUES
(cartes_num_seq.nextval, 2010, 'Postal', 20);
INSERT INTO cartes 
(numero_carta, año, forma_envio, codigo_nen) VALUES
(cartes_num_seq.nextval, 2018, 'Electronica', 21);
INSERT INTO cartes 
(numero_carta, año, forma_envio, codigo_nen) VALUES
(cartes_num_seq.nextval, 2000, 'Postal', 22);
INSERT INTO cartes 
(numero_carta, año, forma_envio, codigo_nen) VALUES
(cartes_num_seq.nextval, 2007, 'Electronica', 23);
INSERT INTO cartes 
(numero_carta, año, forma_envio, codigo_nen) VALUES
(cartes_num_seq.nextval, 2002, 'Postal', 24);
INSERT INTO cartes 
(numero_carta, año, forma_envio, codigo_nen) VALUES
(cartes_num_seq.nextval, 2014, 'Electronica', 25);
INSERT INTO cartes 
(numero_carta, año, forma_envio, codigo_nen) VALUES
(cartes_num_seq.nextval, 2004, 'Postal', 26);
INSERT INTO cartes 
(numero_carta, año, forma_envio, codigo_nen) VALUES
(cartes_num_seq.nextval, 2009, 'Electronica', 27);
INSERT INTO cartes 
(numero_carta, año, forma_envio, codigo_nen) VALUES
(cartes_num_seq.nextval, 2016, 'Postal', 28);
INSERT INTO cartes 
(numero_carta, año, forma_envio, codigo_nen) VALUES
(cartes_num_seq.nextval, 2009, 'Electronica', 29);
INSERT INTO cartes 
(numero_carta, año, forma_envio, codigo_nen) VALUES
(cartes_num_seq.nextval, 2009, 'Postal', 30);
INSERT INTO cartes 
(numero_carta, año, forma_envio, codigo_nen) VALUES
(cartes_num_seq.nextval, 2011, 'Electronica', 11);
INSERT INTO cartes 
(numero_carta, año, forma_envio, codigo_nen) VALUES
(cartes_num_seq.nextval, 2013, 'Postal', 12);
INSERT INTO cartes 
(numero_carta, año, forma_envio, codigo_nen) VALUES
(cartes_num_seq.nextval, 2005, 'Electronica', 13);
INSERT INTO cartes 
(numero_carta, año, forma_envio, codigo_nen) VALUES
(cartes_num_seq.nextval, 2003, 'Postal', 14);
INSERT INTO cartes 
(numero_carta, año, forma_envio, codigo_nen) VALUES
(cartes_num_seq.nextval, 2015, 'Electronica', 15);
INSERT INTO cartes 
(numero_carta, año, forma_envio, codigo_nen) VALUES
(cartes_num_seq.nextval, 2006, 'Postal', 16);
INSERT INTO cartes 
(numero_carta, año, forma_envio, codigo_nen) VALUES
(cartes_num_seq.nextval, 2013, 'Electronica', 17);
INSERT INTO cartes 
(numero_carta, año, forma_envio, codigo_nen) VALUES
(cartes_num_seq.nextval, 2012, 'Postal', 18);
INSERT INTO cartes 
(numero_carta, año, forma_envio, codigo_nen) VALUES
(cartes_num_seq.nextval, 2009, 'Electronica', 19);
INSERT INTO cartes 
(numero_carta, año, forma_envio, codigo_nen) VALUES
(cartes_num_seq.nextval, 2005, 'Postal', 10);
INSERT INTO cartes 
(numero_carta, año, forma_envio, codigo_nen) VALUES
(cartes_num_seq.nextval, 2006, 'Electronica', 9);
INSERT INTO cartes 
(numero_carta, año, forma_envio, codigo_nen) VALUES
(cartes_num_seq.nextval, 2010, 'Postal', 8);
INSERT INTO cartes 
(numero_carta, año, forma_envio, codigo_nen) VALUES
(cartes_num_seq.nextval, 2001, 'Electronica', 7);
INSERT INTO cartes 
(numero_carta, año, forma_envio, codigo_nen) VALUES
(cartes_num_seq.nextval, 2011, 'Postal', 6);
INSERT INTO cartes 
(numero_carta, año, forma_envio, codigo_nen) VALUES
(cartes_num_seq.nextval, 2008, 'Electronica', 5);
/*Pide*/

INSERT INTO PIDE
(id_joguina, numero_carta, codigo_nen, cantidad) VALUES
(1, 15, 30, 1);
INSERT INTO PIDE
(id_joguina, numero_carta, codigo_nen, cantidad) VALUES
(2, 14, 29, 5);
INSERT INTO PIDE
(id_joguina, numero_carta, codigo_nen, cantidad) VALUES
(3, 13, 28, 2);
INSERT INTO PIDE
(id_joguina, numero_carta, codigo_nen, cantidad) VALUES
(4, 12, 27, 10);
INSERT INTO PIDE
(id_joguina, numero_carta, codigo_nen, cantidad) VALUES
(5, 11, 26, 1);
INSERT INTO PIDE
(id_joguina, numero_carta, codigo_nen, cantidad) VALUES
(6, 10, 25, 1);
INSERT INTO PIDE
(id_joguina, numero_carta, codigo_nen, cantidad) VALUES
(7, 9, 24, 6);
INSERT INTO PIDE
(id_joguina, numero_carta, codigo_nen, cantidad) VALUES
(8, 8, 23, 1);
INSERT INTO PIDE
(id_joguina, numero_carta, codigo_nen, cantidad) VALUES
(9, 7, 22, 9);
INSERT INTO PIDE
(id_joguina, numero_carta, codigo_nen, cantidad) VALUES
(10, 6, 21, 7);
INSERT INTO PIDE
(id_joguina, numero_carta, codigo_nen, cantidad) VALUES
(11, 5, 20, 8);
INSERT INTO PIDE
(id_joguina, numero_carta, codigo_nen, cantidad) VALUES
(12, 4, 1, 8);
INSERT INTO PIDE
(id_joguina, numero_carta, codigo_nen, cantidad) VALUES
(13, 3, 2, 10);
INSERT INTO PIDE
(id_joguina, numero_carta, codigo_nen, cantidad) VALUES
(14, 2, 3, 4);
INSERT INTO PIDE
(id_joguina, numero_carta, codigo_nen, cantidad) VALUES
(15, 1, 4, 1);
INSERT INTO PIDE
(id_joguina, numero_carta, codigo_nen, cantidad) VALUES
(16, 30, 5, 6);
INSERT INTO PIDE
(id_joguina, numero_carta, codigo_nen, cantidad) VALUES
(17, 29, 6, 10);
INSERT INTO PIDE
(id_joguina, numero_carta, codigo_nen, cantidad) VALUES
(18, 28, 7, 6);
INSERT INTO PIDE
(id_joguina, numero_carta, codigo_nen, cantidad) VALUES
(19, 27, 8, 4);
INSERT INTO PIDE
(id_joguina, numero_carta, codigo_nen, cantidad) VALUES
(20, 26, 9, 20);
INSERT INTO PIDE
(id_joguina, numero_carta, codigo_nen, cantidad) VALUES
(21, 25, 10, 8);
INSERT INTO PIDE
(id_joguina, numero_carta, codigo_nen, cantidad) VALUES
(22, 24, 19, 10);
INSERT INTO PIDE
(id_joguina, numero_carta, codigo_nen, cantidad) VALUES
(23, 23, 18, 5);
INSERT INTO PIDE
(id_joguina, numero_carta, codigo_nen, cantidad) VALUES
(24, 22, 17, 15);
INSERT INTO PIDE
(id_joguina, numero_carta, codigo_nen, cantidad) VALUES
(25, 21, 16, 20);
INSERT INTO PIDE
(id_joguina, numero_carta, codigo_nen, cantidad) VALUES
(26, 20, 15, 21);
INSERT INTO PIDE
(id_joguina, numero_carta, codigo_nen, cantidad) VALUES
(27, 19, 14, 6);
INSERT INTO PIDE
(id_joguina, numero_carta, codigo_nen, cantidad) VALUES
(28, 18, 13, 7);
INSERT INTO PIDE
(id_joguina, numero_carta, codigo_nen, cantidad) VALUES
(29, 17, 12, 4);
INSERT INTO PIDE
(id_joguina, numero_carta, codigo_nen, cantidad) VALUES
(30, 16, 11, 5);

/*Guardados*/
INSERT INTO guardados 
(id_joguina, nombre_magatzem, reserva_actual) VALUES
(1, 'Magtzem-Barcelona', 50);
INSERT INTO guardados 
(id_joguina, nombre_magatzem, reserva_actual) VALUES
(10, 'Magtzem-Barcelona', 19);
INSERT INTO guardados 
(id_joguina, nombre_magatzem, reserva_actual) VALUES
(16, 'Magtzem-Barcelona', 17);
INSERT INTO guardados 
(id_joguina, nombre_magatzem, reserva_actual) VALUES
(2, 'Magtzem-Barcelona', 23);
INSERT INTO guardados 
(id_joguina, nombre_magatzem, reserva_actual) VALUES
(14, 'Magtzem-Barcelona', 19);
INSERT INTO guardados 
(id_joguina, nombre_magatzem, reserva_actual) VALUES
(21, 'Magtzem-Roma', 36);
INSERT INTO guardados 
(id_joguina, nombre_magatzem, reserva_actual) VALUES
(12, 'Magtzem-Roma', 45);
INSERT INTO guardados 
(id_joguina, nombre_magatzem, reserva_actual) VALUES
(23, 'Magtzem-Roma', 21);
INSERT INTO guardados 
(id_joguina, nombre_magatzem, reserva_actual) VALUES
(3, 'Magtzem-Roma', 13);
INSERT INTO guardados 
(id_joguina, nombre_magatzem, reserva_actual) VALUES
(22, 'Magtzem-Bogota', 11);
INSERT INTO guardados 
(id_joguina, nombre_magatzem, reserva_actual) VALUES
(20, 'Magtzem-Bogota', 12);
INSERT INTO guardados 
(id_joguina, nombre_magatzem, reserva_actual) VALUES
(4, 'Magtzem-Bogota', 9);
INSERT INTO guardados 
(id_joguina, nombre_magatzem, reserva_actual) VALUES
(27, 'Magtzem-Bogota', 8);
INSERT INTO guardados 
(id_joguina, nombre_magatzem, reserva_actual) VALUES
(24, 'Magtzem-BAires', 54);
INSERT INTO guardados 
(id_joguina, nombre_magatzem, reserva_actual) VALUES
(28, 'Magtzem-BAires', 10);
INSERT INTO guardados 
(id_joguina, nombre_magatzem, reserva_actual) VALUES
(26, 'Magtzem-BAires', 28);
INSERT INTO guardados 
(id_joguina, nombre_magatzem, reserva_actual) VALUES
(5, 'Magtzem-BAires', 22);
INSERT INTO guardados 
(id_joguina, nombre_magatzem, reserva_actual) VALUES
(15, 'Magtzem-Caracas', 10);
INSERT INTO guardados 
(id_joguina, nombre_magatzem, reserva_actual) VALUES
(6, 'Magtzem-Caracas', 13);
INSERT INTO guardados 
(id_joguina, nombre_magatzem, reserva_actual) VALUES
(25, 'Magtzem-Caracas', 32);
INSERT INTO guardados 
(id_joguina, nombre_magatzem, reserva_actual) VALUES
(17, 'Magtzem-Caracas', 5);
INSERT INTO guardados 
(id_joguina, nombre_magatzem, reserva_actual) VALUES
(29, 'Magtzem-Caracas', 23);
INSERT INTO guardados 
(id_joguina, nombre_magatzem, reserva_actual) VALUES
(7, 'Magtzem-Lisboa', 11);
INSERT INTO guardados 
(id_joguina, nombre_magatzem, reserva_actual) VALUES
(11, 'Magtzem-Lisboa', 15);
INSERT INTO guardados 
(id_joguina, nombre_magatzem, reserva_actual) VALUES
(30, 'Magtzem-Lisboa', 20);
INSERT INTO guardados 
(id_joguina, nombre_magatzem, reserva_actual) VALUES
(19, 'Magtzem-Lisboa', 10);
INSERT INTO guardados 
(id_joguina, nombre_magatzem, reserva_actual) VALUES
(18, 'Magtzem-Paris', 30);
INSERT INTO guardados 
(id_joguina, nombre_magatzem, reserva_actual) VALUES
(8, 'Magtzem-Paris', 7);
INSERT INTO guardados 
(id_joguina, nombre_magatzem, reserva_actual) VALUES
(13, 'Magtzem-Paris', 65);
INSERT INTO guardados 
(id_joguina, nombre_magatzem, reserva_actual) VALUES
(9, 'Magtzem-Paris', 45);

/*Encarga*/
INSERT INTO encarga
(patge_id, año, nombre_poblacion, nombre_pais) VALUES
(1, 2015, 'Barcelona', 'España');
INSERT INTO encarga
(patge_id, año, nombre_poblacion, nombre_pais) VALUES
(2, 2012, 'Bogota', 'Colombia');
INSERT INTO encarga
(patge_id, año, nombre_poblacion, nombre_pais) VALUES
(3, 2009, 'Paris', 'Francia');
INSERT INTO encarga
(patge_id, año, nombre_poblacion, nombre_pais) VALUES
(4, 2007,'Roma', 'Italia');
INSERT INTO encarga
(patge_id, año, nombre_poblacion, nombre_pais) VALUES
(5, 2017,'Buenos Aires', 'Argentina');
INSERT INTO encarga
(patge_id, año, nombre_poblacion, nombre_pais) VALUES
(6, 2018, 'Caracas', 'Venezuela');
INSERT INTO encarga
(patge_id, año, nombre_poblacion, nombre_pais) VALUES
(7, 2008, 'Lisboa', 'Portugal');

/*corresponde*/
INSERT INTO corresponde
(id_joguina, codigo_nen, año) VALUES
(1, 30, 2015);
INSERT INTO corresponde
(id_joguina, codigo_nen, año) VALUES
(2, 29, 2008);
INSERT INTO corresponde
(id_joguina, codigo_nen, año) VALUES
(3, 28, 2013);
INSERT INTO corresponde
(id_joguina, codigo_nen, año) VALUES
(4, 27, 2011);
INSERT INTO corresponde
(id_joguina, codigo_nen, año) VALUES
(5, 26, 2010);
INSERT INTO corresponde
(id_joguina, codigo_nen, año) VALUES
(6, 25, 2009);
INSERT INTO corresponde
(id_joguina, codigo_nen, año) VALUES
(7, 24, 2011);
INSERT INTO corresponde
(id_joguina, codigo_nen, año) VALUES
(8, 23, 2017);
INSERT INTO corresponde
(id_joguina, codigo_nen, año) VALUES
(9, 22, 2018);
INSERT INTO corresponde
(id_joguina, codigo_nen, año) VALUES
(10, 21, 2003);
INSERT INTO corresponde
(id_joguina, codigo_nen, año) VALUES
(11, 20, 2006);
INSERT INTO corresponde
(id_joguina, codigo_nen, año) VALUES
(12, 1, 2010);
INSERT INTO corresponde
(id_joguina, codigo_nen, año) VALUES
(13, 2, 2011);
INSERT INTO corresponde
(id_joguina, codigo_nen, año) VALUES
(14, 3, 2002);
INSERT INTO corresponde
(id_joguina, codigo_nen, año) VALUES
(15, 4, 2002);
INSERT INTO corresponde
(id_joguina, codigo_nen, año) VALUES
(16, 5, 2015);
INSERT INTO corresponde
(id_joguina, codigo_nen, año) VALUES
(17, 6, 2014);
INSERT INTO corresponde
(id_joguina, codigo_nen, año) VALUES
(18, 7, 2003);
INSERT INTO corresponde
(id_joguina, codigo_nen, año) VALUES
(19, 8, 2012);
INSERT INTO corresponde
(id_joguina, codigo_nen, año) VALUES
(20, 9, 2017);
INSERT INTO corresponde
(id_joguina, codigo_nen, año) VALUES
(21, 10, 2004);
INSERT INTO corresponde
(id_joguina, codigo_nen, año) VALUES
(22, 19, 2016);
INSERT INTO corresponde
(id_joguina, codigo_nen, año) VALUES
(23, 18, 2013);
INSERT INTO corresponde
(id_joguina, codigo_nen, año) VALUES
(24, 17, 2001);
INSERT INTO corresponde
(id_joguina, codigo_nen, año) VALUES
(25, 16, 2002);
INSERT INTO corresponde
(id_joguina, codigo_nen, año) VALUES
(26, 15, 2014);
INSERT INTO corresponde
(id_joguina, codigo_nen, año) VALUES
(27, 14, 2012);
INSERT INTO corresponde
(id_joguina, codigo_nen, año) VALUES
(28, 13, 2001);
INSERT INTO corresponde
(id_joguina, codigo_nen, año) VALUES
(29, 12, 2013);
INSERT INTO corresponde
(id_joguina, codigo_nen, año) VALUES
(30, 11, 2009);

/*View*/
create view view_habilitats_socials as 
select id_joguina, nombre_joguina
from joguina
where id_joguina IN (select id_joguina
from joguina
where capacitat_nen='Habilitats Socials');

create view view_patges_bcn as
select e.patge_id,p.nombre_patge
from encarga e,patges p
where e.patge_id=p.patge_id and 
e.año between 2012 and 2017;




