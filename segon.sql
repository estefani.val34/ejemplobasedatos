/*1.Consulta simple*/
/*Almacenes que hay en Barcelona. nombre_magatzem, direccion, nombre_poblacion, nombre_pais  */
select m.nombre_magatzem, m.direccion
from magatzem m, poblacions p
where p.nombre_magatzem=m.nombre_magatzem
and p.nombre_poblacion='Barcelona';

/*1.Consulta por agrupación*/
/*Dime el total de juguetes que tenemos en todos los almacenes de España(sumame todos los juguetes de todos los almacenes de España)*/
select p.nombre_pais,sum(g.reserva_actual)
from guardados g, poblacions p
where g.nombre_magatzem=p.nombre_magatzem
and p.nombre_pais='España'
group by p.nombre_pais
order by p.nombre_pais

/*Consulta cruzando Tablas*/
/*1.-Los niños buenos el 2015 en Barcelona(Esapña) que juguetes 
han pedido. 
(comportament,año, nombre_poblacion, nombre_pais)
id_juguete,nombre_juguete */
select n.codigo_nen, n.nom,pi.id_joguina,j.nombre_joguina
from nen n, cartes ca, pide pi,joguina j
where  n.codigo_nen=ca.codigo_nen and
ca.codigo_nen=pi.codigo_nen and
pi.id_joguina=j.id_joguina and
n.comportament='buena'and 
n.nombre_poblacion='Barcelona'and
n.nombre_pais='España' and
ca.año=2015;

/*Consulta con subconsultas*/
/*1.-Los niños Barcelona que piden un numero de juguetes, mayor a la media de juguetes que piden los niños en Barcelona*/
select pi.codigo_nen, n.nen, pi.cantidad
from pide pi,cartes ca, nen n
where pi.cantidad>(select round(avg(pi.cantidad),2)
from pide pi, cartes ca, nen n
where pi.numero_carta=ca.numero_carta and
ca.codigo_nen=n.codigo_nen and 
n.nombre_poblacion='Barcelona' and 
n.nombre_poblacion='España') and
pi.numero_carta=ca.numero_carta and
ca.codigo_nen=n.codigo_nen and 
n.nombre_poblacion='Barcelona' and 
n.nombre_poblacion='España';

/*Consulta utilizant UNION*/

/* Los niños que tengan en su nombre una letra A y los niños que contengan una letra E*/

select codigo_nen,nom
from nen
where upper(nom) like '%A%'
UNION
select codigo_nen, nom
from nen
where upper(nom) like '%E%'
order by 1;



















