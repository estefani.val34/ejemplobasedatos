create table joguina
(
id_joguina number(10),
nombre_joguina varchar2(100) not null,
descripcio varchar2(70),
edat number(10) not null, 
constraint jogui_nom_pk primary key (id_joguina),
constraint jogui_edat_ck check(edat<16)
);

create table educatives
(
id_joguina number(10),
capacitat_nen varchar2(30),
constraint jogui_nom_pk1 primary key (id_joguina),
constraint jogui_nom_fk foreign key (id_joguina) references
joguina(id_joguina),
constraint edu_cap_ck check (capacitat_nen in('psicomotricitat', 'autoestima', 'habilitats socials '))
);

create table esportives
(
id_joguina number(10),
tipus_esport varchar2(20),
constraint jogui_nom_pk2 primary key (id_joguina),
constraint jogui_nom_fk1 foreign key (id_joguina) references joguina (id_joguina),
constraint esp_tipus_ck check(tipus_esport in('equip', 'pilota', 'acuàtic'))
);

create table paisos
(
nombre_pais varchar2(70),
nº_nens number(10),
latitud varchar2(10),
longitud varchar2(10),
constraint ps_nom_pk primary key (nombre_pais)
);

create table magatzem 
(
nombre_magatzem varchar2(25),
direccion varchar2(50) not null,
constraint mg_nom_pk primary key (nombre_magatzem)
);

create table patges 
(
patge_id number(10) primary key,
nombre_patge varchar2(50),
idioma varchar2(50) default 'Inglés'
);

create table historic
(
año number(10) default to_char(sysdate,'YYYY'),
constraint hs_año_pk primary key (año),
constraint hs_año_ck check (año>2000)
);

create table poblacions
(
nombre_poblacion varchar2(70) not null unique,
nombre_pais varchar2(70) not null unique,
nombre_magatzem varchar2(25) not null,
constraint pob_nom_pk primary key (nombre_poblacion,nombre_pais),
constraint pob_nomp_fk foreign key (nombre_pais) 
references paisos(nombre_pais),
constraint pob_mag_fk foreign key (nombre_magatzem) 
references magatzem(nombre_magatzem) 

);
create table nen
(
codigo_nen number(10) not null unique,
nom varchar2(50) not null,
data_naixement date default sysdate,
comportament varchar2(70),
nombre_poblacion varchar2(70) not null unique,
nombre_pais varchar2(70) not null unique, 
constraint nen_cod_pk primary key (codigo_nen,nombre_poblacion,nombre_pais),
constraint nen_comp_ck check(comportament in('buena', 'mala', 'normal')),
constraint nen_pob_fk foreign key (nombre_poblacion) references poblacions (nombre_poblacion),
constraint nen_pais_fk foreign key (nombre_pais) references paisos(nombre_pais)
);


create table cartes 
(
numero_carta number(10) not null unique,
año number(10) default to_char(sysdate,'YYYY'),
forma_envio varchar2(15),
codigo_nen number(10) not null unique,
constraint car_num_pk primary key (numero_carta,codigo_nen),
constraint car_form_ck check (forma_envio in('postal' , 'electrònica')),
constraint nen_cod_fk foreign key (codigo_nen)     references nen(codigo_nen)

);


create table equivalente  
(
id_joguina1 number(10) not null unique,
id_joguina2 number(10) not null unique,
constraint eq_jo12_pk primary key (id_joguina1,id_joguina2),
constraint eq_jo1_fk foreign key (id_joguina1)
references joguina(id_joguina),
constraint eq_jo2_fk foreign key (id_joguina2)
references joguina(id_joguina)
);

create table pide 
(
id_joguina number(10) not null unique,
numero_carta number(10) not null unique,
codigo_nen number(10) not null unique,
cantidad number(10),
constraint pid_nom_pk primary key
(id_joguina,numero_carta,codigo_nen),
constraint pid_nom_fk foreign key (id_joguina) 
references joguina(id_joguina),
constraint pid_num_fk foreign key (numero_carta) 
references cartes(numero_carta) ,
constraint pid_cod_fk foreign key (codigo_nen)
references nen(codigo_nen)

);

create table guardados
(
id_joguina number(10) not null unique,
nombre_magatzem varchar2(25) not null unique,
reserva_actual number(10),
constraint gr_nom_pk primary key (id_joguina,nombre_magatzem),
constraint gr_nomj_fk foreign key (id_joguina) 
references joguina(id_joguina),
constraint gr_nomm_fk foreign key (nombre_magatzem)
references magatzem(nombre_magatzem)
);

create table encarga
(
patge_id number(10) not null unique,
año number(10) default to_char(sysdate,'YYYY') not null unique,
nombre_poblacion varchar2(70),
nombre_pais varchar2(70),
constraint en_pt_pk primary key (patge_id, año),
constraint en_pt_fk foreign key (patge_id)
references patges(patge_id),
constraint en_año_fk foreign key (año)
references historic(año),
constraint en_año_ck check (año>2000),
constraint en_nom_fk foreign key (nombre_poblacion)
references poblacions(nombre_poblacion),
constraint en_nomp_fk foreign key (nombre_pais)
references paisos(nombre_pais)
);

create table corresponde
(
id_joguina number(10) not null unique,
codigo_nen number(10) not null unique,
año number(10) default to_char(sysdate,'YYYY') not null unique,
constraint cr_nomj_pk primary key (id_joguina, codigo_nen),
constraint cr_nomj_fk foreign key (id_joguina)
references joguina(id_joguina),
constraint cr_cg_fk foreign key (codigo_nen)
references nen(codigo_nen),
constraint cr_año_fk foreign key (año)
references historic(año),
constraint cr_año_ck check (año>2000)

);

/*VISTAS*/

/*1.-Dime los juguetes que educan las habilidades sociales. id_juguete, nom_jugeute. */

create view view_habilitats_socials as 
select id_joguina, nombre_joguina
from joguina
where id_joguina=(select id_joguina
from educatives
where capacitat_nen='habilitats socials ')


/*2.-Los pages que estaban en Barcelona entre 2001 y 2015 . */

create view view_patges_bcn as
select e.patge_id,p.nombre_patge
from encarga e,patges p
where e.patge_id=p.patge_id and 
e.año between 2001 and 2015 and
e.nombre_poblacion='Barcelona' and e.nombre_pais='España'

/*SECUENCIAS*/
create sequence joguina_id_seq
increment by 1
start with 1
nocycle;

create sequence cartes_num_seq
increment by 1
start with 1
nocycle;

create sequence nen_cod_seq
increment by 1
start with 1
nocycle;

create sequence patge_id_seq
increment by 1
start with 1
nocycle;











